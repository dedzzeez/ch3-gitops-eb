{{- define "MyAppCtx.name" -}}
{{- default .Release.Name | trunc 63 | trimSuffix "-" }}
{{- end }}


{{- define "getAPIURL" -}}
{{- $apiUrl := "" -}}
{{- if .Values.api.ingress.tlsEnabled -}}
  {{- $apiUrl = printf "https://%s" .Values.api.ingress.host | quote -}}
{{- else -}}
  {{- $apiUrl = printf "http://%s" .Values.api.ingress.host | quote -}}
{{- end -}}
{{- $apiUrl -}}
{{- end -}}